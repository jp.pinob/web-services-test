# Servicios Web: Examen práctico

En la base de datos tenemos un catálogo de libros que debemos administrar mediante un servicio REST. 

Las columnas son:
- `id`: int autoincrement
- `nombre`: varchar(200)
- `autor`: varchar(200)
- `prestado_a`: varchar(200) | null
- `fecha_creacion`: date
- `fecha_modificacion`: date

Se espera que se implemente un CRUD para la administración de este recurso. Se necesitan endpoints para:
- Crear un registro
- Consultar todos los registros
- Consultar registro por id
- Modificar registro por id
- Eliminar registro por id

La mensajería debe ser JSON, y debe indicarse apropiadamente en los headers HTTP (produces, consumes..)

Ejemplo de respuesta para la consulta de libro por id: 
```
{
    "id": 1,
    "nombre": "La insportable levedad del ser",
    "autor": "Milan Kundera",
    "prestado_a": "Juan Pablo Pino",
    "fecha_creacion": "Thu Jan 13 2022 11:09:06 GMT-0500 (-05)",
    "fecha_modificacion": "Thu Jan 13 2022 11:09:06 GMT-0500 (-05)"
}
```

- Usar parametros en path con `@PathParam` 
- Usar métodos HTTP apropiados para el CRUD (GET, POST, PUT|PATCH, DELETE)
- Implementar validaciones de los datos enviados al servicio (tal vez con Bean Validation: import javax.validation.constraints...)
- Hacer el mapeo de las entidades como se sienta más comodo. Puede usar JPA (@Entity y EntityManager) o hacer consultas directas a la base. 
- Se puede obtener acceso al DataSource (para obtener una conexión a la base de datos) de la siguiente manera:
```
Context initialContext = new InitialContext();
        pool = (DataSource) initialContext.lookup("jdbc/BOOKS");
        initialContext.close();
```