/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.hiper.rest.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.builder.api.AppenderComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder;
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory;
import org.apache.logging.log4j.core.config.builder.api.LayoutComponentBuilder;
import org.apache.logging.log4j.core.config.builder.api.RootLoggerComponentBuilder;
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration;

/**
 *
 * @author Luis Bravo
 */
public class LogConfiguration {

    private static LogConfiguration log = null;

    public static LogConfiguration getInstance() {
        if (log == null) {
            log = new LogConfiguration();
        }
        return log;
    }

    /**
     * Reference: https://www.baeldung.com/log4j2-programmatic-config
     */
    private LogConfiguration() {
        ConfigurationBuilder<BuiltConfiguration> builder = ConfigurationBuilderFactory.newConfigurationBuilder();

        /**
         * Configure Appender Console and RollingFile
         */
        AppenderComponentBuilder console = builder.newAppender("stdout", "Console");
        console.addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT);

        /**
         * Configure Layout
         */
        LayoutComponentBuilder standard = builder.newLayout("PatternLayout");
        standard.addAttribute("pattern", "%d{yyyy-MM-dd HH:mm:ss}    Tipo: %-5p    Clase: %c{1}    Linea Numero: %L    Mensaje: %m%n");

        console.add(standard);

        /**
         * Adding console y rolling file to builder
         */
        builder.add(console);

        /**
         * Initialize configuration
         */
        System.out.println(builder.toXmlConfiguration());

        Configurator.initialize(builder.build());
    }

    public Logger setName(String name) {
        return LogManager.getLogger(name);
    }
}
