package com.hiper.rest.exam.resources;

import com.hiper.rest.util.LogConfiguration;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
//import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
/**
 *
 * @author 
 */
@Path("books")
@Stateless
public class BooksResource {
    
    private static final Logger LOGGER = LogConfiguration.getInstance().setName(BooksResource.class.getName());
    
    static {
        LOGGER.info("Carga Configuracion de Log");
    }
    
    @GET
    public Response findAllBooks(){
        JSONObject json = new JSONObject();
        json.put("test", "test");
        LOGGER.error("Called findAllBooks");

        return Response
                .ok(json.toString())
                .build();
    }
}
